TARGET_EXEC := littleVoxelEngine
OBJ_DIR := obj
SRC_DIR := src

################################

SRCS := $(shell find $(SRC_DIR) -name *.cpp -or -name *.c)
OBJS := $(SRCS:%=$(OBJ_DIR)/%.o)
DEPS := $(shell find $(OBJ_DIRS) -name *.d)

################################

LOCAL_INC_DIRS := $(shell find $(SRC_DIR) -type d)
LOCAL_INC_DIRS_FLAGS := $(addprefix -I,$(LOCAL_INC_DIRS))
INC_FLAGS := $(LOCAL_INC_DIRS_FLAGS)

CXX = clang++
CC = clang
CPPFLAGS := -g -Wall -std=c++17 $(INC_FLAGS) -MMD -MP
CFLAGS := -g -std=c11 $(INC_FLAGS) -MMD -MP
LINKER = clang++
LIBS = -lGL -lglfw -lX11 -ldl
LDFLAGS := $(LIBS)

################################

# linking
$(TARGET_EXEC): $(OBJS)
	$(LINKER) $(OBJS) -o $@ $(LDFLAGS)

# c compilation
$(OBJ_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# cpp compilation
$(OBJ_DIR)/%.cpp.o: %.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

################################
.PHONY: clean
clean:
	rm -r $(OBJ_DIR)/*
	rm $(TARGET_EXEC)

################################

include $(DEPS)
