#pragma once

#include <string>
#include "LVE_Voxel.h"
#include "LVE_Texture.h"
#include "LVE_Shader.h"
#include "LVE_Material.h"

enum class LVE_BlockType
{
	Stone = 0,
	Grass = 1
};

class LVE_Block 
{
public:
	LVE_Block() = default;

	bool IsActive() const { return myIsActive; }
	void SetActive(bool aIsActive) { myIsActive = aIsActive; }

	void SetType(LVE_BlockType aBlockType);

	void Cleanup() {};
	void Draw() const { LVE_Voxel::GetInstance()->Draw(); }
	unsigned int GetTextureId() const;

	const LVE_Material& GetMaterial() const { return myMaterial; }

private: 
	bool myIsActive = true;
	LVE_BlockType myType = LVE_BlockType::Stone;
	LVE_Material myMaterial;

public:
	[[nodiscard]] static bool GetShaderId(LVE_ShaderId& aShaderIdOut);
	static void SetShaderId(const LVE_ShaderId aShaderId) { ourShaderId = aShaderId; }
	static LVE_ShaderKey GetShaderKey() { return ourShaderKey; }
private:
	inline static const LVE_ShaderKey ourShaderKey {"./src/shaders/basicBlock.vert", "./src/shaders/basicBlock.frag"};
	inline static LVE_ShaderId ourShaderId = -1;
};


void LVE_Block::SetType(LVE_BlockType aBlockType) 
{
	myType = aBlockType;

	switch(myType)
	{
		case LVE_BlockType::Stone:
		{
			//white plastic stone
			myMaterial.myAmbient = glm::vec3(0.2);
			myMaterial.myDiffuse = glm::vec3(0.55);
			myMaterial.mySpecular = glm::vec3(0.7);
			myMaterial.myShininess = 0.25f;
		}
			break;

		case LVE_BlockType::Grass:
		{
			//green plastic grass
			myMaterial.myAmbient = glm::vec3(0.2);
			myMaterial.myDiffuse = glm::vec3(0.1, 0.35, 0.1);
			myMaterial.mySpecular = glm::vec3(0.45, 0.55, 0.45);
			myMaterial.myShininess = 0.25f;
		}
			break;

		default:
			//default material
			break;
	}
}

unsigned int LVE_Block::GetTextureId() const
{
	unsigned int textureId = -1;

	switch(myType)
	{
		case LVE_BlockType::Stone:
			LVE_Texture::GetTexture("data/stone.png", textureId);
			break;

		case LVE_BlockType::Grass:
			LVE_Texture::GetTexture("data/grass.png", textureId);
			break;

		default:
			LVE_Texture::GetTexture("data/grid.png", textureId);
			break;
	}

	return textureId;
}

bool LVE_Block::GetShaderId(LVE_ShaderId& aShaderIdOut)
{
	if(ourShaderId == static_cast<LVE_ShaderId>(-1))
	{
		return false;
	}
	
	aShaderIdOut = ourShaderId;
	return true;
}
