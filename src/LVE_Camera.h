#pragma once

#include "glm/glm.hpp"

class LVE_Camera
{
public:
	LVE_Camera();

	void SetPosition(const glm::vec3& aPos) { myIsLookAtDirty = true; myPosition = aPos; }
	void SetFront(const glm::vec3& aFront);
	void SetUp(const glm::vec3& anUp) { myIsLookAtDirty = true; myUp = anUp; }

	void IncrementWorldPosition(const glm::vec3& aWorldIncrement) { SetPosition(myPosition + aWorldIncrement); }
	void Walk(const glm::vec3& aFrontWalkStep);

	const glm::mat4& GetLookAtMatrix();

private:
	bool myIsLookAtDirty;
	glm::vec3 myPosition;
	glm::vec3 myFront;
	glm::vec3 myUp;
	glm::mat4 myLookAt;
};

////////////////////////////////
// LVE_Camera Implementation
////////////////////////////////

inline LVE_Camera::LVE_Camera()
	: myIsLookAtDirty(false)
	, myPosition(0.0f, 0.0f, 0.0f)
	, myFront(0.0f, 0.0f, -1.0f)
	, myUp(0.0f, 1.0f, 0.0f)
	, myLookAt(1.0f)
{}

inline const glm::mat4& LVE_Camera::GetLookAtMatrix()
{
	if(myIsLookAtDirty == true)
	{
		myLookAt = glm::lookAt(myPosition, myPosition + myFront, myUp);
		myIsLookAtDirty = false;
	}

	return myLookAt;
}

void LVE_Camera::SetFront(const glm::vec3& aFront)
{ 
	myIsLookAtDirty = true;
	myFront = aFront;
}

void LVE_Camera::Walk(const glm::vec3& aFrontWalkStep)
{
	myIsLookAtDirty = true;

	myPosition += aFrontWalkStep.z * myFront;
	myPosition += aFrontWalkStep.x * glm::normalize(glm::cross(myFront, myUp));
	myPosition += aFrontWalkStep.y * myUp;
}
