#pragma once

#include "LVE_Block.h"

constexpr int CHUNK_DIMENSION_SIZE_X = 16;
constexpr int CHUNK_DIMENSION_SIZE_Y = 16;
constexpr int CHUNK_DIMENSION_SIZE_Z = 16;
constexpr int BLOCKS_NUMBER = 
		CHUNK_DIMENSION_SIZE_X 
	* CHUNK_DIMENSION_SIZE_Y 
	* CHUNK_DIMENSION_SIZE_Z;

class LVE_Chunk
{
public:
	LVE_Chunk();

	void Cleanup();
	void SetWorldTransform(const glm::mat4& aWorldTransform) { myWorldTransform = aWorldTransform; }

	void Draw(int aBlockIndex) const;
	unsigned int GetTextureId(int aBlockIndex) const;
	const LVE_Material& GetBlockMaterial(int aBlockIndex) const;
	glm::mat4 GetBlockWorldTransform(int aBlockIndex) const;

private:
	void FillCoordinatesFromIndex(int aBlockIndex, int& anXOut, int& aYOut, int& aZOut) const;

private:
	LVE_Block myBlocks[CHUNK_DIMENSION_SIZE_X][CHUNK_DIMENSION_SIZE_Y][CHUNK_DIMENSION_SIZE_Z];
	glm::mat4 myWorldTransform;
};

LVE_Chunk::LVE_Chunk()
{

	for(int x = 0; x < CHUNK_DIMENSION_SIZE_X; ++x)
		for(int y = 0; y < CHUNK_DIMENSION_SIZE_Y; ++y)
			for(int z = 0; z < CHUNK_DIMENSION_SIZE_Z; ++z)
			{
				myBlocks[x][y][z].SetActive(rand() % 2);
				if(y > CHUNK_DIMENSION_SIZE_Y / 2)
					myBlocks[x][y][z].SetType(LVE_BlockType::Grass);
			}
}

void LVE_Chunk::Cleanup()
{
	for(int x = 0; x < CHUNK_DIMENSION_SIZE_X; ++x)
		for(int y = 0; y < CHUNK_DIMENSION_SIZE_Y; ++y)
			for(int z = 0; z < CHUNK_DIMENSION_SIZE_Z; ++z)
				myBlocks[x][y][z].Cleanup();
}

void LVE_Chunk::Draw(int aBlockIndex) const
{
	int x; int y;	int z;
	FillCoordinatesFromIndex(aBlockIndex, x, y, z);
	if(myBlocks[x][y][z].IsActive())
		myBlocks[x][y][z].Draw();
}

glm::mat4 LVE_Chunk::GetBlockWorldTransform(int aBlockIndex) const
{
	int x; int y;	int z;
	FillCoordinatesFromIndex(aBlockIndex, x, y, z);
	glm::mat4 voxelTransform(myWorldTransform);
	return glm::translate(voxelTransform, glm::vec3(x, y, z));
}

unsigned int LVE_Chunk::GetTextureId(int aBlockIndex) const
{
	int x; int y;	int z;
	FillCoordinatesFromIndex(aBlockIndex, x, y, z);
	return myBlocks[x][y][z].GetTextureId();
}

const LVE_Material& LVE_Chunk::GetBlockMaterial(int aBlockIndex) const
{
	int x; int y;	int z;
	FillCoordinatesFromIndex(aBlockIndex, x, y, z);
	return myBlocks[x][y][z].GetMaterial();
}

void LVE_Chunk::FillCoordinatesFromIndex(int aBlockIndex, int& anXOut, int& aYOut, int& aZOut) const
{
	anXOut = aBlockIndex % CHUNK_DIMENSION_SIZE_X;
	aYOut = (aBlockIndex / CHUNK_DIMENSION_SIZE_X) % CHUNK_DIMENSION_SIZE_Y;
	aZOut = aBlockIndex / (CHUNK_DIMENSION_SIZE_X * CHUNK_DIMENSION_SIZE_Y);
}
