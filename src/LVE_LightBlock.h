#pragma once

#include <cmath>
#include "LVE_Voxel.h"
#include "LVE_Shader.h"
#include "LVE_Material.h"

class LVE_LightBlock 
{
public:
	LVE_LightBlock() = default;

	bool IsActive() const { return myIsActive; }
	void SetActive(bool aIsActive) { myIsActive = aIsActive; }

	void Cleanup() {};
	void Draw() const { LVE_Voxel::GetInstance()->Draw(); }

	const glm::mat4& GetWorldTransform() const { return myWorldTransform; }

	void SetOscillating(bool aIsOscillating) { myIsOscillating = aIsOscillating; }
	void SetOscillationAxis(const glm::vec3& anOscillationAxis) { myOscillationAxis = anOscillationAxis; }
	void SetOscillationSpeedRadiansPerSecond(const float anOscillationSpeedRadiansPerSecond) { myOscillationSpeedRadiansPerSecond = anOscillationSpeedRadiansPerSecond; }

	void Update(float anElapsedFrameTime);

	void SetColour(const glm::vec3& aColour) { myColour = aColour; }
	const glm::vec3& GetColour() const { return myColour; }

	const LVE_LightProps& GetProperties() const { return myLightProperties; }

private: 
	bool myIsActive = true;
	bool myIsOscillating = true;
	float myOscillationAngle = 0.0f;
	float myOscillationSpeedRadiansPerSecond = 0.314f;
	glm::mat4 myWorldTransform = glm::mat4(1.0);
	glm::vec3 myOscillationAxis = glm::vec3(0);
	glm::vec3 myColour = glm::vec3(0);
	LVE_LightProps myLightProperties;

public:
	[[nodiscard]] static bool GetShaderId(LVE_ShaderId& aShaderIdOut);
	static void SetShaderId(const LVE_ShaderId aShaderId) { ourShaderId = aShaderId; }
	static LVE_ShaderKey GetShaderKey() { return ourShaderKey; }
private:
	inline static const LVE_ShaderKey ourShaderKey {"./src/shaders/lightBlock.vert", "./src/shaders/lightBlock.frag"};
	inline static LVE_ShaderId ourShaderId = -1;
};

const int lightDistance = 20;

bool LVE_LightBlock::GetShaderId(LVE_ShaderId& aShaderIdOut)
{
	if(ourShaderId == static_cast<LVE_ShaderId>(-1))
	{
		return false;
	}
	
	aShaderIdOut = ourShaderId;
	return true;
}

void LVE_LightBlock::Update(float anElapsedFrameTime)
{
	if(myIsOscillating == false) return;

	myOscillationAngle += myOscillationSpeedRadiansPerSecond * anElapsedFrameTime;
	if(myOscillationAngle > (2.0f * 3.14f))
		myOscillationAngle -= (2.0f * 3.14f);

	float oscillationValue = std::sin(myOscillationAngle);
	glm::vec3 pos = lightDistance * oscillationValue * myOscillationAxis;

	myWorldTransform = glm::mat4(1.0);
	myWorldTransform = glm::translate(myWorldTransform, pos);
}
