#pragma once

#include <iostream>

class LVE_Logger
{
public:
	inline static void Log(const char* aMessage)
	{
		if(ourIsLoggingEnabled)
		{
			std::cout << aMessage << std::endl;
		}
	}

public:
		inline static bool ourIsLoggingEnabled = false;
};
