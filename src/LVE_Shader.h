#pragma once

#include "glad/glad.h" //glad for all the gl headers
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include "glm/glm.hpp"

#include "LVE_Logger.h"

class LVE_Shader;

using LVE_ShaderId = unsigned int;
using LVE_ShaderKey = std::pair<const char*, const char*>;
using LVE_ShaderKeyToIdMap = std::map<LVE_ShaderKey, LVE_ShaderId>;
using LVE_ShaderIdToShaderMap = std::map<LVE_ShaderId, LVE_Shader>;

class LVE_ShaderRepo
{
public:
	[[nodiscard]] bool GetShaderFromId(const LVE_ShaderId& aShaderId
			, LVE_Shader& aShaderOut);

	[[nodiscard]] bool GetShaderIdFromKey(const LVE_ShaderKey& aShaderKey
			, LVE_ShaderId& aShaderIdOut);

	[[nodiscard]] bool LoadShader(const LVE_ShaderKey& aShaderKey
			, LVE_ShaderId& aShaderIdOut);

public:
	static LVE_ShaderRepo* const GetInstance();
private:
	LVE_ShaderRepo();
	inline static LVE_ShaderRepo* ourInstance = nullptr;

private:
	LVE_ShaderKeyToIdMap myShaderKeyToIdMap;
	LVE_ShaderIdToShaderMap myShaderIdToShaderMap;
};

///////////////////////
// LVE_Shader Definition
///////////////////////
class LVE_Shader
{
private:
	static const unsigned int INVALID_SHADER_PROGRAM_ID = -1;
	static const unsigned int INVALID_UNIFORM_LOCATION = -1;

	enum class LVE_ShaderLoadingFormat
	{
		VERTEX = 0,
		FRAGMENT = 1,
	};

public:
	LVE_Shader() {}
	LVE_Shader(const LVE_ShaderKey& aShaderKey);
	bool IsReadyForUse() const { return myShaderProgramId != INVALID_SHADER_PROGRAM_ID; }
	void Use() const { if(IsReadyForUse()) glUseProgram(myShaderProgramId); }
	void Cleanup() { if(IsReadyForUse()) glDeleteProgram(myShaderProgramId); };
	LVE_ShaderId GetShaderId() const { return myShaderProgramId; }

	template<class T>
	void SetUniform(const char* aUniformName, const T& aValue) const;

private:
	static bool LoadShader(const char* aShaderPath, LVE_ShaderLoadingFormat aShaderLoadingFormat, LVE_ShaderId& aShaderIdOut);

	static bool LoadShaderSourceFromPath(const char* aShaderPath, std::string& aShaderSourceOut);
	bool IsUniformOkToUse(const char* aUniformName, unsigned int& aUniformLocationOut) const;

	template<class T>
	void SetUniformInternal(unsigned int aUniformLocation, const T& aValue) const;

public:
	LVE_ShaderId myShaderProgramId = INVALID_SHADER_PROGRAM_ID;
};

///////////////////////
// LVE_ShaderRepo Implementation
///////////////////////

LVE_ShaderRepo* const LVE_ShaderRepo::GetInstance()
{
	if(ourInstance == nullptr)
		ourInstance = new LVE_ShaderRepo();

	return ourInstance;
}

LVE_ShaderRepo::LVE_ShaderRepo()
	: myShaderKeyToIdMap()
	, myShaderIdToShaderMap()
{
}

bool LVE_ShaderRepo::LoadShader(const LVE_ShaderKey& aShaderKey, LVE_ShaderId& aShaderIdOut)
{
	LVE_ShaderKeyToIdMap::iterator shaderIter = myShaderKeyToIdMap.find(aShaderKey);
	if (shaderIter != myShaderKeyToIdMap.end())
	{
		//shader already exists
		LVE_Logger::Log("WARNING: asked to load shader that already exists!");
		return false;
	}

	LVE_Shader shader(aShaderKey);
	if(shader.IsReadyForUse() == false) 
	{
		LVE_Logger::Log("ERROR: Failed to initialize shader");
		return false;
	}

	aShaderIdOut = shader.GetShaderId();
	myShaderKeyToIdMap[aShaderKey] = aShaderIdOut;
	myShaderIdToShaderMap[aShaderIdOut] = shader;
	return true;
}

bool LVE_ShaderRepo::GetShaderFromId(const LVE_ShaderId& aShaderId
		, LVE_Shader& aShaderOut)
{
	LVE_ShaderIdToShaderMap::iterator shaderIter = myShaderIdToShaderMap.find(aShaderId);

	if (shaderIter != myShaderIdToShaderMap.end())
	{
		aShaderOut = myShaderIdToShaderMap[aShaderId];
		return true;
	}

	//did not find a shader in the map, constructing it
	LVE_Logger::Log("ERROR tried to get shader from Id that is unrecognised");
	return false;
}

bool LVE_ShaderRepo::GetShaderIdFromKey(const LVE_ShaderKey& aShaderKey
		, LVE_ShaderId& aShaderIdOut)
{
	LVE_ShaderKeyToIdMap::iterator shaderIter = myShaderKeyToIdMap.find(aShaderKey);

	if (shaderIter != myShaderKeyToIdMap.end())
	{
		aShaderIdOut = std::get<1>(*shaderIter);
		return true;
	}

	//did not find a shader in the map, constructing it
	LVE_Logger::Log("ERROR tried to get shaderId from key that is unrecognised");
	aShaderIdOut = -1;
	return false;
}

///////////////////////
// LVE_Shader Implementation
///////////////////////

inline LVE_Shader::LVE_Shader(const LVE_ShaderKey& aShaderKey)
{
	const char* aVertexShaderPath = std::get<0>(aShaderKey);
	const char* aFragmentShaderPath = std::get<1>(aShaderKey);
	// create and compile the vertex shader
	unsigned int vertexShader = -1;
	if(LoadShader(aVertexShaderPath, LVE_ShaderLoadingFormat::VERTEX, vertexShader) == false)
		return;

	// create and compile the fragment shader
	unsigned int fragmentShader = -1;
	if(LoadShader(aFragmentShaderPath, LVE_ShaderLoadingFormat::FRAGMENT, fragmentShader) == false)
		return;

	// create the shader program and link up the shaders
	LVE_ShaderId shaderProgramId = glCreateProgram();
	glAttachShader(shaderProgramId, vertexShader);
	glAttachShader(shaderProgramId, fragmentShader);
	glLinkProgram(shaderProgramId);
	int shaderBuildSuccess = 0;
	glGetProgramiv(shaderProgramId, GL_LINK_STATUS, &shaderBuildSuccess);
	if(shaderBuildSuccess == false)
	{
		char shaderBuildInfoLog[512];
		glGetProgramInfoLog(shaderProgramId, 512, nullptr, shaderBuildInfoLog);

		LVE_Logger::Log("ERROR::SHADER::PROGRAM::LINKING_FAILED");

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
		return;
	}

	// shaders are ready to use
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	myShaderProgramId = shaderProgramId;
}

template<class T>
inline void LVE_Shader::SetUniform(const char* aUniformName, const T& aValue) const
{
	unsigned int uniformLocation = INVALID_UNIFORM_LOCATION;
	if(IsUniformOkToUse(aUniformName, uniformLocation))
		SetUniformInternal<T>(uniformLocation, aValue);
}

inline bool LVE_Shader::LoadShader(const char* aShaderPath, LVE_ShaderLoadingFormat aShaderLoadingFormat, LVE_ShaderId& aShaderIdOut)
{
	aShaderIdOut = -1;

	std::string shaderSource;
	if(LoadShaderSourceFromPath(aShaderPath, shaderSource) == false)
	{
		LVE_Logger::Log("ERROR::SHADER::VERTEX::FILE_NOT_READ_SUCCESSFULLY");
		return false;
	}
	
	// create and compile the shader
	switch(aShaderLoadingFormat)
	{
		case LVE_ShaderLoadingFormat::VERTEX:
			aShaderIdOut = glCreateShader(GL_VERTEX_SHADER);
			break;

		case LVE_ShaderLoadingFormat::FRAGMENT:
			aShaderIdOut = glCreateShader(GL_FRAGMENT_SHADER);
			break;

		default:
			break;
	}

	const char* shaderCode = shaderSource.c_str();
	glShaderSource(aShaderIdOut, 1, &shaderCode, nullptr);
	glCompileShader(aShaderIdOut);

	int shaderBuildSuccess = 0;
	glGetShaderiv(aShaderIdOut, GL_COMPILE_STATUS, &shaderBuildSuccess);
	if(shaderBuildSuccess == false)
	{
		char shaderBuildInfoLog[512];
		glGetShaderInfoLog(aShaderIdOut, 512, nullptr, shaderBuildInfoLog);
		LVE_Logger::Log("ERROR::SHADER::VERTEX::COMPILATION_FAILED");
		LVE_Logger::Log(shaderBuildInfoLog);

		glDeleteShader(aShaderIdOut);
		return false;
	}

	return true;
}

inline bool LVE_Shader::LoadShaderSourceFromPath(const char* aShaderPath, std::string& aShaderSourceOut)
{
	std::ifstream shaderFile;
	shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	// read shaders from the path
	try
	{
		//open files
		shaderFile.open(aShaderPath);
		//read buffer into stream
		std::stringstream shaderStream;
		shaderStream << shaderFile.rdbuf();
		//close files
		shaderFile.close();
		//convert streams into strings
		aShaderSourceOut = shaderStream.str();
	}
	catch(const std::ifstream::failure& e)
	{
		return false;
	}

	return true;
}

inline bool LVE_Shader::IsUniformOkToUse(const char* aUniformName, unsigned int& aUniformLocationOut) const
{
	if(IsReadyForUse())
	{
		unsigned int uniformLocation = glGetUniformLocation(myShaderProgramId, aUniformName);
		if(uniformLocation != INVALID_UNIFORM_LOCATION)
		{
			aUniformLocationOut = uniformLocation;
			return true;
		}
		else
		{
			LVE_Logger::Log("ERROR::SHADER::UNIFORM_NOT_FOUND");
			return false;
		}
	}
	else
	{
		LVE_Logger::Log("ERROR::SHADER::TRYING_TO_SET_UNIFORM_ON_PROGRAM_NOT_IN_USE");
		return false;
	}
}

template<>
inline void LVE_Shader::SetUniformInternal(unsigned int aUniformLocation, const bool& aValue) const
{
	glUniform1i(aUniformLocation, aValue ? 1 : 0);
}

template<>
inline void LVE_Shader::SetUniformInternal(unsigned int aUniformLocation, const int& aValue) const
{
	glUniform1i(aUniformLocation, aValue);
}

template<>
inline void LVE_Shader::SetUniformInternal(unsigned int aUniformLocation, const float& aValue) const
{
	glUniform1f(aUniformLocation, aValue);
}

template<>
inline void LVE_Shader::SetUniformInternal(unsigned int aUniformLocation, const glm::mat4& aValue) const
{
	glUniformMatrix4fv(aUniformLocation, 1, GL_FALSE, glm::value_ptr(aValue));
}

template<>
inline void LVE_Shader::SetUniformInternal(unsigned int aUniformLocation, const glm::vec3& aValue) const
{
	glUniform3fv(aUniformLocation, 1, glm::value_ptr(aValue));
}
