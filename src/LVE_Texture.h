#pragma once

#include <unordered_map>
#include "stb/stb_image.h"
#include "LVE_Logger.h"

class LVE_Texture
{
public:
	enum class LVE_TextureLoadingResult
	{
		FAILED = 0,
		SUCCESS = 1,
	};

	enum class LVE_TextureLoadingFormat
	{
		RGB = 0,
		RGBA = 1,
	};

public:
	static LVE_TextureLoadingResult GetTexture(
				const char* aTexturePath
			, unsigned int& aTextureIdOut)
	{
		return GetTexture(aTexturePath, aTextureIdOut, LVE_TextureLoadingFormat::RGBA);
	}

	static LVE_TextureLoadingResult GetTexture(
				const char* aTexturePath
			, unsigned int& aTextureIdOut
			, LVE_TextureLoadingFormat aLoadingFormat)
	{
		LVE_TextureLoadingResult result = LVE_TextureLoadingResult::FAILED;

		auto foundIterator = ourLoadedTexturesMap.find(aTexturePath);
		if(foundIterator != ourLoadedTexturesMap.end())
		{
			//found the texture, just return it
			aTextureIdOut = foundIterator->second;
			result = LVE_TextureLoadingResult::SUCCESS;
		}
		else
		{
			LVE_Logger::Log("requested texture not found, loading it");
			glGenTextures(1, &aTextureIdOut);
			int width, height, channelCount;
			stbi_set_flip_vertically_on_load(true);
			if(unsigned char* textureData = stbi_load(aTexturePath, &width, &height, &channelCount, 0))
			{
				glBindTexture(GL_TEXTURE_2D, aTextureIdOut);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				GLint loadingFormat = (aLoadingFormat == LVE_TextureLoadingFormat::RGB) ? GL_RGB : GL_RGBA;

				glTexImage2D(
							GL_TEXTURE_2D // texture target, load into whatever is bound here
						, 0 // mipmap level we want to create for
						, loadingFormat // format we want to store the texture
						, width // width of source image
						, height // height of source image
						, 0 // legacy stuff?
						, loadingFormat // format of source image
						, GL_UNSIGNED_BYTE // byte format of source image... we loaded as unsigned chars, so byte
						, textureData
						); // the source data
				glGenerateMipmap(GL_TEXTURE_2D); // will generate mipmaps, or you can repeat the above and load manually

				glBindTexture(GL_TEXTURE_2D, 0);

				stbi_image_free(textureData);

				ourLoadedTexturesMap.insert(std::pair<const char*, unsigned int>(aTexturePath, aTextureIdOut));

				result = LVE_TextureLoadingResult::SUCCESS;
			}
		}

		return result;
	}

private:
	inline static std::unordered_map<const char*, unsigned int> ourLoadedTexturesMap = std::unordered_map<const char*, unsigned int>();
};

