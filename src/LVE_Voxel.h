#pragma once

#include <cstdlib>
#include <vector>
#include "glad/glad.h"

namespace LVE_VoxelHelper
{
	void ConfigureAttribute(const int anAttributeLocation
			, const int anAttributeSize
			, const int aStrideSize
			, int& anOffset)
	{
		glVertexAttribPointer(
				anAttributeLocation	// which attribute we want to configure. aTexCoord is at location 2 (see vertex shader)
			, anAttributeSize 
			, GL_FLOAT // what type are we sending in
			, GL_FALSE // do we want the values normalised [-1.0 - 1.0]
			, aStrideSize
			, (void*)(anOffset * sizeof(float)) 
			);
		glEnableVertexAttribArray(anAttributeLocation); // now that we've configured the texCoords input, enable it
		anOffset += anAttributeSize;
	}
}

class LVE_Voxel
{
public:
	void Draw() const;
	void Cleanup();

public:
	static const LVE_Voxel* const GetInstance();
private:
	inline static LVE_Voxel* ourInstance = nullptr;
	LVE_Voxel();

private:
	unsigned int myVertexBufferObject;
	unsigned int myElementBufferObject;
	unsigned int myMeshVertexArrayObject;
	unsigned int myVertexCount;
};

const LVE_Voxel* const LVE_Voxel::GetInstance()
{
	if(ourInstance == nullptr)
		ourInstance = new LVE_Voxel();

	return ourInstance;
}


LVE_Voxel::LVE_Voxel() 
{
	// create the buffer to prepare for the meshVertices
	glGenBuffers(1, &myVertexBufferObject);
	glGenBuffers(1, &myElementBufferObject);
	// create the vertex array
	glGenVertexArrays(1, &myMeshVertexArrayObject);
	glBindVertexArray(myMeshVertexArrayObject);

	// the vertices
	float meshVertices[] = 
	{  // positions x3,      colour x3,          texCoord x2,   normals x3
		-0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 0.0f,    0.0f,  0.0f, -1.0f,
		 0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,    0.0f,  0.0f, -1.0f,
		 0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 1.0f,    0.0f,  0.0f, -1.0f,
		 0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 1.0f,    0.0f,  0.0f, -1.0f,
		-0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,    0.0f,  0.0f, -1.0f,
		-0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 0.0f,    0.0f,  0.0f, -1.0f,

		-0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 0.0f,    0.0f,  0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,    0.0f,  0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 1.0f,    0.0f,  0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 1.0f,    0.0f,  0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,    0.0f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 0.0f,    0.0f,  0.0f, 1.0f,

		-0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,   -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 1.0f,   -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 0.0f,   -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,   -1.0f,  0.0f,  0.0f,

		 0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,    1.0f,  0.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 1.0f,    1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,    1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,    1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 0.0f,    1.0f,  0.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,    1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,    0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 1.0f,    0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,    0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,    0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 0.0f,    0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,    0.0f, -1.0f,  0.0f,

		-0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,    0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 1.0f,    0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,    0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   1.0f, 0.0f,    0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 0.0f,    0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 1.0f,   0.0f, 1.0f,    0.0f,  1.0f,  0.0f
	};

	myVertexCount = sizeof(meshVertices);

	glBindBuffer(GL_ARRAY_BUFFER, myVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, myVertexCount, meshVertices, GL_STATIC_DRAW);

	// the indices
	unsigned int meshPolyVertexIndices[] = 
	{
		0, 1, 2,
		3, 4, 5,
		6, 7, 8,
		9, 10, 11,
		12, 13, 14,
		15, 16, 17,
		18, 19, 20,
		21, 22, 23,
		24, 25, 26,
		27, 28, 29,
		30, 31, 32,
		33, 34, 35
	};
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myElementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(meshPolyVertexIndices), meshPolyVertexIndices, GL_STATIC_DRAW);

	// link vertex attributes
	const int stride = 11;// the stride is 3pos + 3col + 2texCoord + 3normals
	const size_t strideSize = stride * sizeof(float);
	int offset = 0;// start offset, which is 0 since we're the first here
	
	LVE_VoxelHelper::ConfigureAttribute(0, 3, strideSize, offset);// position
	LVE_VoxelHelper::ConfigureAttribute(1, 3, strideSize, offset);// colour
	LVE_VoxelHelper::ConfigureAttribute(2, 2, strideSize, offset);// texoords
	LVE_VoxelHelper::ConfigureAttribute(3, 3, strideSize, offset);// normals
}

inline void LVE_Voxel::Draw() const
{
	//bind the VAB
	glBindVertexArray(myMeshVertexArrayObject);

	// draw using the elements buffer
	glDrawElements(
			GL_TRIANGLES // draw mode
		, myVertexCount // draw 36 vertices
		, GL_UNSIGNED_INT // what kind of data we're sending in
		, 0 // offset of start
	);
}

inline void LVE_Voxel::Cleanup()
{
	glDeleteVertexArrays(1, &myMeshVertexArrayObject);
	glDeleteBuffers(1, &myVertexBufferObject);
	glDeleteBuffers(1, &myElementBufferObject);
}

