#include <cmath>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <vector>

#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "LVE_Logger.h"
#include "LVE_Shader.h"
#include "LVE_Chunk.h"
#include "LVE_Camera.h"
#include "LVE_LightBlock.h"

constexpr unsigned int DEFAULT_WIDTH = 800;
constexpr unsigned int DEFAULT_HEIGHT = 600;
float currentWidth = (float)DEFAULT_WIDTH;
float currentHeight = (float)DEFAULT_HEIGHT;

using LVE_Chunks = std::vector<LVE_Chunk>;
using LVE_Lights = std::vector<LVE_LightBlock>;

void FramebufferSizeCallback(GLFWwindow* aWindow, int aWidth, int aHeight)
{
	glViewport(0, 0, aWidth, aHeight);
	currentWidth = (float)aWidth;
	currentHeight = (float)aHeight;
}

float MOUSE_SENSITIVTY = 0.1f;
float cameraPitch = 0.0f;
float cameraYaw = 0.0f;
bool isFirstMouseCall = true;
bool isInCursorMode = false;

void ProcessMouseInputCallback(GLFWwindow* aWindow, double anXPos, double aYPos)
{
	static double lastMouseX;
	static double lastMouseY;

	if(isFirstMouseCall || isInCursorMode)
	{
		lastMouseX = anXPos;
		lastMouseY = aYPos;
		isFirstMouseCall = false;
	}

	if(isInCursorMode) return;

	double movementX = (anXPos - lastMouseX) * MOUSE_SENSITIVTY;
	double movementY = (lastMouseY - aYPos) * MOUSE_SENSITIVTY;

	cameraPitch += movementY;
	cameraPitch = std::clamp(cameraPitch, -89.0f, 89.0f);
	cameraYaw += movementX;

	lastMouseX = anXPos;
	lastMouseY = aYPos;
}

float horizontalSpeedPerSecond = 5.0f;
float verticalSpeedPerSecond = 5.0f;

void ProcessInput(GLFWwindow* aWindow, LVE_Camera& aCamera, float anElapsedFrameTime)
{
	//////////////// check if we need to close
	if(glfwGetKey(aWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(aWindow, true);
	//////////////// 

	//////////////// alt gives the cursor
	isInCursorMode = glfwGetKey(aWindow, GLFW_KEY_LEFT_ALT) == GLFW_PRESS;
	if(isInCursorMode)
		glfwSetInputMode(aWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	else
		glfwSetInputMode(aWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//////////////// 

	//////////////// set the camera position
	glm::vec3 posIncrement(0.0f);
	const float horizontalSpeed = horizontalSpeedPerSecond * anElapsedFrameTime;
	const float verticalSpeed = verticalSpeedPerSecond * anElapsedFrameTime;

	if(glfwGetKey(aWindow, GLFW_KEY_D) == GLFW_PRESS)
		posIncrement.x = horizontalSpeed;
	else if(glfwGetKey(aWindow, GLFW_KEY_A) == GLFW_PRESS)
		posIncrement.x = -1.0f * horizontalSpeed;

	if(glfwGetKey(aWindow, GLFW_KEY_W) == GLFW_PRESS)
		posIncrement.z = horizontalSpeed;
	else if(glfwGetKey(aWindow, GLFW_KEY_S) == GLFW_PRESS)
		posIncrement.z = -1.0f * horizontalSpeed;

	if(glfwGetKey(aWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		posIncrement.y = verticalSpeed;
	else if(glfwGetKey(aWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
		posIncrement.y = -1.0f * verticalSpeed;

	aCamera.Walk(posIncrement);
	//////////////// 

	//////////////// set the camera front
	if(isInCursorMode == false)
	{
		glm::vec3 cameraFront;
		cameraFront.x = cos(glm::radians(cameraPitch)) * cos(glm::radians(cameraYaw));
		cameraFront.y = sin(glm::radians(cameraPitch));
		cameraFront.z = cos(glm::radians(cameraPitch)) * sin(glm::radians(cameraYaw));
		cameraFront = glm::normalize(cameraFront);
		aCamera.SetFront(cameraFront);
	}
	//////////////// 
}

void UpdateLights(LVE_Lights& someLights
		, const float aLightOscillationSpeedRadiansPerSecond
		, const bool anIsOscillating
		, const float anElapsedFrameTime)
{
	for(LVE_LightBlock& light : someLights)
	{
		light.SetOscillationSpeedRadiansPerSecond(aLightOscillationSpeedRadiansPerSecond);
		light.SetOscillating(anIsOscillating);
		light.Update(anElapsedFrameTime);
	}
}

void DrawLights(const LVE_Lights& someLights
		, const glm::mat4& aViewMatrix
		, const glm::mat4& aProjectionMatrix)
{
	LVE_ShaderId shaderId = -1;
	if(LVE_LightBlock::GetShaderId(shaderId) == false) return;

	LVE_Shader shader;
	if(LVE_ShaderRepo::GetInstance()->GetShaderFromId(shaderId, shader) == false) return;

	LVE_Logger::Log("using light block shader");
	shader.Use();
	shader.SetUniform<glm::mat4>("view", aViewMatrix);
	shader.SetUniform<glm::mat4>("projection", aProjectionMatrix);

	for(const LVE_LightBlock& light : someLights)
	{
		const glm::mat4& modelMatrix = light.GetWorldTransform();
		shader.SetUniform<glm::mat4>("model", modelMatrix);

		const glm::vec3& colour = light.GetColour();
		shader.SetUniform<glm::vec3>("colour", colour);
		light.Draw();
	}
}

void DrawChunks(const LVE_Chunks& someChunks
		, const glm::mat4& aViewMatrix
		, const glm::mat4& aProjectionMatrix
		, const bool aIsTextureRendering
		, const int aNumBlocksPerChunkToDraw
		, const LVE_Lights& someLights)
{
	LVE_ShaderId shaderId = -1;
	if(LVE_Block::GetShaderId(shaderId) == false) return;

	LVE_Shader shader;
	if(LVE_ShaderRepo::GetInstance()->GetShaderFromId(shaderId, shader) == false) return;

	LVE_Logger::Log("using block shader");
	shader.Use();
	shader.SetUniform<glm::mat4>("view", aViewMatrix);
	shader.SetUniform<glm::mat4>("projection", aProjectionMatrix);
	shader.SetUniform<bool>("isTexturedRendering", aIsTextureRendering);
	shader.SetUniform<int>("ourTexture0", 0);// tell the shader which texture is which sampler
	shader.SetUniform<glm::vec3>("aViewPos", glm::vec3(aViewMatrix[3]));

	glm::mat4 lightTransform = someLights[0].GetWorldTransform();
	glm::vec3 lightPosition = glm::vec3(lightTransform[3]);
	shader.SetUniform<glm::vec3>("light.pos", lightPosition);
	shader.SetUniform<glm::vec3>("light.colour",someLights[0].GetColour());
	const LVE_LightProps& lightProps = someLights[0].GetProperties();
	shader.SetUniform<float>("light.ambient", lightProps.myAmbient);
	shader.SetUniform<float>("light.diffuse", lightProps.myDiffuse);
	shader.SetUniform<float>("light.specular", lightProps.mySpecular);

	unsigned int boundTextureId = -1;
	const int numChunks = someChunks.size();
	for(int chunkIndex = 0; chunkIndex < numChunks; ++chunkIndex)
	{
		const LVE_Chunk& chunk = someChunks[chunkIndex];
		for(int blockIndex = 0; blockIndex < aNumBlocksPerChunkToDraw; ++blockIndex)
		{
			// load textures
			{
				const unsigned int textureId = chunk.GetTextureId(blockIndex);
				if(boundTextureId != textureId)
				{
					LVE_Logger::Log("binding a texture");
					glActiveTexture(GL_TEXTURE0); // activate texture 0, in case the driver doesnt do it
					glBindTexture(GL_TEXTURE_2D, textureId);
					boundTextureId = textureId;
				}
			}

			const glm::mat4& modelMatrix = chunk.GetBlockWorldTransform(blockIndex);
			shader.SetUniform<glm::mat4>("model", modelMatrix);

			const LVE_Material& material = chunk.GetBlockMaterial(blockIndex);
			shader.SetUniform<glm::vec3>("material.ambient", material.myAmbient);
			shader.SetUniform<glm::vec3>("material.diffuse", material.myDiffuse);
			shader.SetUniform<glm::vec3>("material.specular", material.mySpecular);
			shader.SetUniform<float>("material.shininess", material.myShininess);

			chunk.Draw(blockIndex);
		}
	}
}

int main(int argc, char** args)
{
	//////////////// init glfw
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	////////////////

	//////////////// create the window
	GLFWwindow* window = glfwCreateWindow(DEFAULT_WIDTH, DEFAULT_HEIGHT, "LearnOpenGL", nullptr, nullptr);
	if(window == nullptr)
	{
		LVE_Logger::Log("ERROR::GLFW::FAILED_TO_CREATE_WINDOW");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);
	glfwSetCursorPosCallback(window, ProcessMouseInputCallback);
	////////////////

	//////////////// init glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		LVE_Logger::Log("ERROR::GLAD::FAILED_TO_INITIALIZE");
		return -1;
	}
	////////////////
	
	//////////////// init imgui
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& imguiIO = ImGui::GetIO(); (void)imguiIO;
	ImGui::StyleColorsDark();
	const char* glsl_version = "#version 150";
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);
	//////////////// 
	
	//////////////// init rendering pipeline
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	////////////////

	//////////////// init world chunks
	const int numChunks = 1;
	LVE_Chunks chunks;
	chunks.reserve(numChunks);

	srand(time(0));
	for(int i = 0; i < numChunks; ++i)
	{
		LVE_Chunk chunk;
		glm::mat4 worldTransform;
		worldTransform = glm::mat4(1.0f);
		worldTransform = glm::translate(worldTransform, glm::vec3(i * CHUNK_DIMENSION_SIZE_X, 0, 0));
		chunk.SetWorldTransform(worldTransform);
		chunks.push_back(chunk);
	}
	
	//init block shader
	LVE_ShaderKey blockShaderKey = LVE_Block::GetShaderKey();
	LVE_ShaderId blockShaderId = -1;
	if(LVE_ShaderRepo::GetInstance()->LoadShader(blockShaderKey, blockShaderId))
		LVE_Block::SetShaderId(blockShaderId);
	////////////////
	
	/////////////// init lights
	const int numLights = 3;
	LVE_Lights lights;
	lights.reserve(numLights);

	LVE_LightBlock redLight;
	redLight.SetOscillationAxis(glm::vec3(1.0, 1.0, 0));
	redLight.SetColour(glm::vec3(1.0, 1.0, 1.0));
	lights.push_back(redLight);

	LVE_LightBlock greenLight;
	greenLight.SetOscillationAxis(glm::vec3(0, 1.0, 1.0));
	greenLight.SetColour(glm::vec3(0.0, 1.0, 0.0));
	lights.push_back(greenLight);

	LVE_LightBlock blueLight;
	blueLight.SetOscillationAxis(glm::vec3(1.0, 0, 1.0));
	blueLight.SetColour(glm::vec3(0.0, 0.0, 1.0));
	lights.push_back(blueLight);
	
	//init light shader
	LVE_ShaderKey lightShaderKey = LVE_LightBlock::GetShaderKey();
	LVE_ShaderId lightShaderId = -1;
	if(LVE_ShaderRepo::GetInstance()->LoadShader(lightShaderKey, lightShaderId))
		LVE_LightBlock::SetShaderId(lightShaderId);
	///////////////

	//setup the camera
	LVE_Camera mainCamera;
	glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	mainCamera.SetFront(cameraFront);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	mainCamera.SetUp(up);
	glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 1.0f);
	mainCamera.SetPosition(cameraPos);
	////////////////

	//////////////// imgui render
	int windowWidth = 0;
	int windowHeight = 0;
	glfwGetWindowSize(window, &windowWidth, &windowHeight);
	//set position of imgui as top left corner with some imguiControlsMargins
	ImVec2 imguiControlsMargins = ImVec2(10,10);
	ImGui::SetNextWindowPos(imguiControlsMargins, ImGuiCond_Always);
	//////////////// 

	//////////////// main loop for window
	bool isWireframeRendering = false;
	bool isTexturedRendering = true;
	bool isOscillatingLights = true;
	float lightOscillationSpeedRadiansPerSecond = 0.314f;
	bool isVSync = true;
	double previousFrameTime = glfwGetTime();
	float fov = 45.0f;
	int numBlocksPerChunkToDraw = 1;
	bool isSteppedFrames = false;
	bool isSteppedFramePassed = true;

	while(glfwWindowShouldClose(window) == false)
	{
		double currentFrameTime = glfwGetTime();
		double elapsedFrameTime = currentFrameTime - previousFrameTime;
		previousFrameTime = currentFrameTime;
		double fps = 1.0 / elapsedFrameTime;

		glfwSwapInterval(isVSync ? 1 : 0);

		{ // input management
			ProcessInput(window, mainCamera, elapsedFrameTime);
		}

		{ //update
			float updateElapsedFrameTime = elapsedFrameTime;
			if(isSteppedFrames)
			{
				updateElapsedFrameTime = isSteppedFramePassed ? 0.0f : 1.0f / 60.0f;
			}
			isSteppedFramePassed = true;

			UpdateLights(lights, lightOscillationSpeedRadiansPerSecond, isOscillatingLights, updateElapsedFrameTime);
		}

		{ // rendering 
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glPolygonMode(GL_FRONT_AND_BACK, isWireframeRendering ? GL_LINE : GL_FILL);

			//set up matrices
			glm::mat4 viewMatrix = mainCamera.GetLookAtMatrix();
			glm::mat4 projectionMatrix = glm::mat4(1.0f);
			projectionMatrix = glm::perspective(glm::radians(fov), (float)currentWidth / (float)currentHeight, 0.1f, 100.0f);

			DrawLights(lights, viewMatrix, projectionMatrix);

			DrawChunks(chunks, viewMatrix, projectionMatrix, isTexturedRendering, numBlocksPerChunkToDraw, lights);

			//////////////////////////////// begin drawing an imgui window
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

			{
				ImGui::Begin("Controls", NULL, ImGuiWindowFlags_AlwaysAutoResize);

				ImGui::Dummy(ImVec2(0.0f, 1.0f));
				ImGui::Checkbox("Stepped Frames", &isSteppedFrames);

				if(ImGui::Button("Step the frame"))
				{
					isSteppedFramePassed = false;
				}

				ImGui::Text("FPS = %f", fps);
				ImGui::Checkbox("VSync", &isVSync);
				ImGui::Checkbox("Render in Wireframe", &isWireframeRendering);
				ImGui::Checkbox("Render textures", &isTexturedRendering);
				ImGui::SliderFloat("FOV", &fov, 20.0f, 90.0f);
				ImGui::SliderFloat("mouse sensitivity", &MOUSE_SENSITIVTY, 0.01f, 0.5f);
				ImGui::SliderFloat("horizontal speed per second", &horizontalSpeedPerSecond, 0.5f, 5.0f);
				ImGui::SliderFloat("vertical speed per second", &verticalSpeedPerSecond, 0.5f, 5.0f);
				ImGui::SliderInt("numBlocksPerChunkToDraw", &numBlocksPerChunkToDraw, 1, BLOCKS_NUMBER);
				ImGui::Checkbox("Logging", &LVE_Logger::ourIsLoggingEnabled);
				ImGui::Checkbox("Oscillating Lights", &isOscillatingLights);
				ImGui::SliderFloat("Light Oscillation Speed", &lightOscillationSpeedRadiansPerSecond, 0.0314f, 3.14f * 2.0f);

				ImGui::End();
			}

			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
			////////////////////////////////

			// swap the buffers and display
			glfwSwapBuffers(window);
		}

		glfwPollEvents();
	}
	////////////////
	
	//////////////// cleanup
	for(int i = 0; i < numChunks; ++i)
	{
		chunks[i].Cleanup();
	}
	////////////////

	//////////////// imgui cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	//////////////// 
	
	glfwTerminate();
	return 0;
}
