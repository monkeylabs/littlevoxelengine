#version 330 core

in vec3 ourColour;
in vec2 texCoord;
in vec3 normal;
in vec3 FragPos;

uniform vec3 aViewPos;

uniform int isTexturedRendering;
uniform sampler2D ourTexture0;

struct Material
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};
uniform Material material;

struct Light
{
	vec3 pos;
	vec3 colour;
	float ambient;
	float diffuse;
	float specular;
};
uniform Light light;

out vec4 FragColour;

void main()
{
	if(isTexturedRendering != 0)
		FragColour = texture(ourTexture0, texCoord);
	else
		FragColour = vec4(ourColour, 1.0);

	//ambient lighting component
	vec3 ambient = light.ambient * material.ambient * light.colour;

	//diffuse lighting component
	vec3 norm = normalize(normal);
	vec3 lightDir = normalize(light.pos - FragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = (light.diffuse * diff * material.diffuse) * light.colour;

	//specular lighting component
	vec3 viewDir = normalize(aViewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess * 128);
	vec3 specular = (light.specular * material.specular * spec) * light.colour;

	vec4 result = vec4((ambient + diffuse + specular), 1.0);
	FragColour = result * FragColour;
}
