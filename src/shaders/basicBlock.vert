#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColour;
layout (location = 2) in vec2 aTexCoord;
layout (location = 3) in vec3 aNormal;

uniform int isTexturedRendering;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 ourColour;
out vec2 texCoord;
out vec3 normal;
out vec3 FragPos;

void main()
{
	gl_Position = projection * view * model * vec4(aPos, 1.0);
	ourColour = aColour;
	texCoord = aTexCoord;
	normal = aNormal;
	FragPos = vec3(model * vec4(aPos, 1.0));
}

